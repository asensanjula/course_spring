package io.java.asen.Controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController
{

    @RequestMapping("/hello")
    public String sayHi()
    {
        return "Hello Asen";
    }

    @RequestMapping("/hello0")
    public String sayHell()
    {
        return "Hello Asen";
    }
}
