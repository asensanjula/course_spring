package io.java.asen.Services;

import io.java.asen.Dto.Topic;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class TopicService
{
    private List<Topic> topics = new ArrayList<>( Arrays.asList(
            new Topic( "Spring", "Spring Framework", "Spring Description" ),
            new Topic( "Java", "Java Framework", "Java Description" ),
            new Topic( "JavaScript", "JavaScript Framework", "JavaScript Description" )
    ) );

    public List<Topic> getAllTopics()
    {
        return topics;
    }

    public Topic getTopic( String id )
    {
        return topics.stream().filter( topic -> topic.getId().equalsIgnoreCase( id ) ).findFirst().get();
    }

    public void addTopic( Topic topic )
    {
        topics.add( topic );
    }
}
